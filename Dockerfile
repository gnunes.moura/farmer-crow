FROM node:14-alpine

WORKDIR /usr/src/app
COPY . .

RUN npm ci --only=production
RUN ls -lah
ENV NODE_ENV production
CMD [ "npm", "run", "start" ]
