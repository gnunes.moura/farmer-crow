const request = require('supertest');
const app = require('./index');

describe('API minimal function', () => {
  it('healthcheck route return now and nowUTC', async () => {
    const res = await request(app)
      .get('/healthcheck');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('now');
    expect(res.body).toHaveProperty('nowUTC');
  });

  it('apollo graphql attached as a express route', async () => {
    const res = await request(app)
      .post('/graphql')
      .send({
        query: 'query { __typename }',
      })
      .set('content-type', 'application/json');
    expect(res.statusCode).toEqual(200);
  });

  it('query temperature and relative humidity empty on start', async () => {
    const res = await request(app)
      .post('/graphql')
      .send({
        query: `
          query ExampleQuery {
            temperatureToday {
              id
            }
            relativeHumidityToday {
              id
            }
          }
      `,
      })
      .set('content-type', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res.body.data.temperatureToday.length).toEqual(0);
    expect(res.body.data.relativeHumidityToday.length).toEqual(0);
  });

  it('postRelativeHumidityRead accept a RelativeHumidityReadInput', async () => {
    const res = await request(app)
      .post('/graphql')
      .send({
        query: `
          mutation PostRelativeHumidityReadMutation($read: RelativeHumidityReadInput!) {
            postRelativeHumidityRead(read: $read) {
              percentage,
              timestamp,
              collector { id }
            }
          }
        `,
        variables: {
          read: {
            collector: '1',
            percentage: 60.9,
            timestamp: 1635980252134,
          },
        },
      })
      .set('content-type', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res).toHaveProperty('body.data.postRelativeHumidityRead');
    const { collector, timestamp, percentage } = res.body.data.postRelativeHumidityRead;
    expect(collector.id).toEqual('1');
    expect(timestamp).toEqual(1635980252134);
    expect(percentage).toEqual(60.9);
  });

  it('postTemperatureRead accept a TemperatureReadInput', async () => {
    const res = await request(app)
      .post('/graphql')
      .send({
        query: `
          mutation PostTemperatureReadMutation($read: TemperatureReadInput!) {
            postTemperatureRead(read: $read) {
              id
              timestamp
              celsius
              collector { id }
            }
          }
        `,
        variables: {
          read: {
            collector: '1',
            celsius: 27.1,
            timestamp: 1635980252134,
          },
        },
      })
      .set('content-type', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res).toHaveProperty('body.data.postTemperatureRead');

    const { collector, timestamp, celsius } = res.body.data.postTemperatureRead;
    expect(collector.id).toEqual('1');
    expect(timestamp).toEqual(1635980252134);
    expect(celsius).toEqual(27.1);
  });

  it('query temperature and relative humidity stored', async () => {
    const res = await request(app)
      .post('/graphql')
      .send({
        query: `
          query ExampleQuery {
            temperatureToday {
              id
            }
            relativeHumidityToday {
              id
            }
          }
      `,
      })
      .set('content-type', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res.body.data.temperatureToday.length).toEqual(1);
    expect(res.body.data.relativeHumidityToday.length).toEqual(1);
  });
});
